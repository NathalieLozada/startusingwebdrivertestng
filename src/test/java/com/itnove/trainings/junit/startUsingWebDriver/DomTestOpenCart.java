package com.itnove.trainings.junit.startUsingWebDriver;

import com.itnove.trainings.junit.startUsingWebDriver.pages.searchPage.ResultsPage;
import com.sun.xml.internal.ws.util.QNameMap;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;


/**
 * Unit test for simple App.
 */
public class DomTestOpenCart extends BaseTest {

    @Test
    public void DoomExempleTestOpenCart() throws InterruptedException {

        driver.navigate().to("https://the-internet.herokuapp.com/challenging_dom");
        //FORMA CORTA CON BUCLE

        List<WebElement> botonesCategorias = driver.findElements(By.xpath(".//div[@class='example']/div/div/div[1]/a"));
        System.out.println(botonesCategorias.size());
        for (int i = 0; i < botonesCategorias.size(); i++) {
            WebElement botonesIterar = driver.findElement(By.xpath(".//div[@class='example']/div/div/div[1]/a[" + (i + 1) + "]"));
            botonesIterar.click();

            WebElement canvas = driver.findElementByXPath(".//div[@class='row']/div/div/div/div/div/div/div/canvas");
            canvas.click();
            Thread.sleep(3000);
        }
    }
}
        /* FORMA LARGA

        WebElement bar = driver.findElementByXPath(".//div[@class='example']/div/div/div/a[1]");
        bar.click();
        Thread.sleep(3000);

        WebElement foo = driver.findElementByXPath(".//div[@class='example']/div/div/div/a[2]");
        foo.click();
        Thread.sleep(3000);

        WebElement quz = driver.findElementByXPath(".//div[@class='example']/div/div/div/a[3]");
        quz.click();
        Thread.sleep(3000);

        WebElement canvas = driver.findElementByXPath(".//div[@class='row']/div/div/div/div/div/div/div/canvas");
        canvas.click();
        Thread.sleep(3000);

        ResultsPage resultsPage = new ResultsPage(driver);
    }
}*/
