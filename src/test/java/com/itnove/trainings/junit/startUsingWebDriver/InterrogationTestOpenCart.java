package com.itnove.trainings.junit.startUsingWebDriver;

import com.itnove.trainings.junit.startUsingWebDriver.pages.searchPage.ResultsPage;
import org.junit.Test;

import static org.junit.Assert.assertTrue;


/**
 * Unit test for simple App.
 */
public class InterrogationTestOpenCart extends BaseTest {

    @Test
    public void InterrogationTestOpenCart() throws InterruptedException {
        driver .get("http://opencart.votarem.lu/");
        assertTrue(driver.getTitle().contains("Your Store"));
        Thread.sleep(3000);
        assertTrue(driver.getPageSource().contains("OpenCart"));
        Thread.sleep(3000);
    }
}
