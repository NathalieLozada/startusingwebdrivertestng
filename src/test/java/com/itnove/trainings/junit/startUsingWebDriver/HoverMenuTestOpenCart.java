package com.itnove.trainings.junit.startUsingWebDriver;

import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import static org.junit.Assert.assertTrue;


/**
 * Unit test for simple App.
 */
public class HoverMenuTestOpenCart extends BaseTest {

    @Test
    public void UserSuiteTestOpenCart() throws InterruptedException {
        driver .get("http://opencart.votarem.lu/");

        Actions hover = new Actions(driver);

        WebElement ninot = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[1]/a");
        hover.moveToElement(ninot).build().perform();
        Thread.sleep(3000);

        WebElement ninot1 = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[1]/div/div/ul/li[1]/a");
        hover.moveToElement(ninot1).build().perform();
        Thread.sleep(3000);

        WebElement ninot2 = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[1]/div/a");
        hover.moveToElement(ninot2).build().perform();
        ninot2.click();
        Thread.sleep(3000);
        WebElement container = driver.findElementByXPath(".//*[@id='product-category']");
        assertTrue(container.isDisplayed());

        WebElement ninot3 = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[2]/a");
        hover.moveToElement(ninot3).build().perform();
        Thread.sleep(3000);

        WebElement ninot4 = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[2]/div/div/ul/li[1]/a");
        hover.moveToElement(ninot4).build().perform();
        Thread.sleep(3000);

        WebElement ninot5 = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[2]/div/div/ul/li[2]/a");
        hover.moveToElement(ninot5).build().perform();
        Thread.sleep(3000);

        WebElement ninot6 = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[2]/div/div/ul/li[2]/a");
        hover.moveToElement(ninot6).build().perform();
        Thread.sleep(3000);

        WebElement ninot7 = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[2]/div/a");
        hover.moveToElement(ninot7).build().perform();
        ninot7.click();
        Thread.sleep(3000);
        WebElement laptops = driver.findElementByXPath(".//*[@id='product-category']/ul/li[2]/a");
        assertTrue(laptops.isDisplayed());

        WebElement ninot8 = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[3]/a");
        hover.moveToElement(ninot8).build().perform();
        Thread.sleep(3000);

        WebElement ninot9 = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[3]/div/div/ul/li[1]/a");
        hover.moveToElement(ninot9).build().perform();
        Thread.sleep(3000);

        WebElement ninot10 = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[3]/div/div/ul/li[2]/a");
        hover.moveToElement(ninot10).build().perform();
        Thread.sleep(3000);

        WebElement ninot11 = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[3]/div/div/ul/li[3]/a");
        hover.moveToElement(ninot11).build().perform();
        Thread.sleep(3000);

        WebElement ninot12 = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[3]/div/div/ul/li[4]/a");
        hover.moveToElement(ninot12).build().perform();
        Thread.sleep(3000);

        WebElement ninot13 = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[3]/div/div/ul/li[5]/a");
        hover.moveToElement(ninot13).build().perform();
        Thread.sleep(3000);

        WebElement ninot14 = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[3]/div/a");
        hover.moveToElement(ninot14).build().perform();
        ninot14.click();
        Thread.sleep(3000);
        WebElement components = driver.findElementByXPath(".//*[@id='content']/h2");
        assertTrue(components.isDisplayed());

        WebElement tablet = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[4]/a");
        hover.moveToElement(tablet).build().perform();
        Thread.sleep(3000);

        WebElement software = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[5]/a");
        hover.moveToElement(software).build().perform();
        Thread.sleep(3000);

        WebElement phone = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[6]/a");
        hover.moveToElement(phone).build().perform();
        Thread.sleep(3000);

        WebElement camera = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[7]/a");
        hover.moveToElement(camera).build().perform();
        Thread.sleep(3000);

        WebElement mp3 = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[8]/a");
        hover.moveToElement(mp3).build().perform();
        Thread.sleep(3000);

        WebElement test11 = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[8]/div/div/ul[1]/li[1]/a");
        hover.moveToElement(test11).build().perform();
        Thread.sleep(3000);

        WebElement test12 = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[8]/div/div/ul[1]/li[2]/a");
        hover.moveToElement(test12).build().perform();
        Thread.sleep(3000);

        WebElement test13 = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[8]/div/div/ul[1]/li[3]/a");
        hover.moveToElement(test13).build().perform();
        Thread.sleep(3000);

        WebElement test14 = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[8]/div/div/ul[1]/li[4]/a");
        hover.moveToElement(test14).build().perform();
        Thread.sleep(3000);

        WebElement test15 = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li[8]/div/div/ul[1]/li[5]/a");
        hover.moveToElement(test15).build().perform();
        Thread.sleep(3000);


    }


}
